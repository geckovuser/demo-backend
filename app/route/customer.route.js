module.exports = function(app) {
 
    const customers = require('../controller/customer.controller.js');
 
    // Create a new Customer
    app.post('/customers', customers.create);
 
    // Retrieve all Customer
    app.get('/customers', customers.findAll);
 
    // Retrieve a single Customer by Id
    app.get('/customers/:customerId', customers.findById);
 
    // Update a Customer with Id
    app.put('/customers', customers.update);
 
    // Delete a Customer with Id
    app.delete('/customers/:customerId', customers.delete);
}